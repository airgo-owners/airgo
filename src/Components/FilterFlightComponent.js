import React from 'react';


export default class FilterFlightComponent extends React.Component {


    constructor() {
        super()
        this.state = { searchedValue: '' }
    }

    onSearch = (event) => {
        this.props.setFilter(event.target.value)
    }

    render() {
        return (
            <div class="input-group">
                <input type="text" class="form-control rounded" placeholder="Search" aria-label="Search"
                    aria-describedby="search-addon" onChange={this.onSearch} value={this.props.filter} />

            </div>

        )
    }

}