
import React from 'react';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import BookFlightComponent from './BookFlightComponent';




export default class FlightCardComponent extends React.Component {



  render() {

    const logo = `../Images/${this.props.flightcard.company}.png`


    return (
      <Container fluid className="mainComponent">
        <Row>

          <Col><h1 className="air-name"> </h1>
            <img className='logo' src={logo} alt="" /></Col>
          <Col xs={8}>
            <div className="info">
              <div className='departure-info'>
                <h1 className="departure-hour">{this.props.flightcard.departureHour}</h1>
                <h1 className='departure-location'>{this.props.flightcard.departureLocation}</h1>
                <h1 className='departure-date'>{this.props.flightcard.departureDate}</h1>

              </div>

              <p className="fly-separator"></p>

              <div className='arrival-info'>
                <h1 className='arrival-hour'>{this.props.flightcard.arrivalHour}</h1>
                <h1 className='arrival-location'>{this.props.flightcard.arrivalLocation}</h1>
                <h1 className='arrival-date'>{this.props.flightcard.arrivalDate}</h1>

              </div>
            </div>
          </Col>
          <Col>
            <BookFlightComponent flights={this.props.flightcard.seats} flightId={this.props.flightcard.id} ></BookFlightComponent>
            <h1 className='price'>price: {this.props.flightcard.price}$</h1>
            <h1 className='available-seats'>seats: {this.props.flightcard.seats}</h1>
          </Col>
        </Row>
      </Container>



    )
  }

}