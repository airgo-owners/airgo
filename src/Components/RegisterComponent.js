import React from 'react';
import './RegisterComponent.css';
import Form from 'react-bootstrap/Form';
import Col from 'react-bootstrap/Col'
import Button from 'react-bootstrap/Button'
import axios from 'axios';

export default class RegisterComponent extends React.Component {


    constructor() {
        super()
        this.state = {
            firstName: null,
            lastName: null,
            phone: null,
            email: null,
            address: null,
            password: null,
            succes: null,
        }
        this.handleChange = this.handleChange.bind(this)
        this.sentPostRequest = this.sentPostRequest.bind(this)
    }


    sentPostRequest() {


        const information = { firstName: this.state.firstName, lastName: this.state.lastName, phone: this.state.phone, email: this.state.email, address: this.state.address, password: this.state.password }
        axios.post(`http://localhost:8080/api/users`, information, { withCredentials: true })
            .then(response => this.setState({ succes: true }))
            .catch(err => this.setState({ succes: false }))
    }

    handleChange(event) {
        this.setState({ [event.target.name]: event.target.value });
    }



    render() {

        return (
            <div className="bodyRegister">
                <Form className="container">

                    <div className="register-title">
                        <Form.Label className="register-label" >
                            Register
                    </Form.Label>
                    </div>
                    <Form.Row>
                        <Form.Group as={Col} controlId="formGridFirstName">
                            <Form.Label className="form-label">First Name</Form.Label>
                            <Form.Control name="firstName" type="text" placeholder="Enter first name" onChange={this.handleChange} />
                        </Form.Group>

                        <Form.Group as={Col} controlId="formGridLastName">
                            <Form.Label className="form-label">Last Name</Form.Label>
                            <Form.Control name="lastName" type="text" placeholder="Enter last name" onChange={this.handleChange} />
                        </Form.Group>
                    </Form.Row>
                    <Form.Row>
                        <Form.Group as={Col} controlId="formGridEmail">
                            <Form.Label className="form-label">Email</Form.Label>
                            <Form.Control name="email" type="email" placeholder="Enter email" onChange={this.handleChange} />
                        </Form.Group>

                        <Form.Group as={Col} controlId="formGridPassword">
                            <Form.Label className="form-label">Password</Form.Label>
                            <Form.Control name="password" type="password" placeholder="Password" onChange={this.handleChange} />
                        </Form.Group>
                    </Form.Row>
                    <Form.Row>
                        <Form.Group as={Col} controlId="formGridPhoneNumber">
                            <Form.Label className="form-label">Phone number</Form.Label>
                            <Form.Control name="phone" type="text" placeholder="Enter phone number" onChange={this.handleChange} />
                        </Form.Group>
                    </Form.Row>
                    <Form.Row>
                        <Form.Group as={Col} controlId="formGridAddress">
                            <Form.Label className="form-label">Address </Form.Label>
                            <Form.Control name="address" placeholder="Country,City,Street,Number" onChange={this.handleChange} />
                        </Form.Group>
                    </Form.Row>
                    <Form.Row>
                        <Button as={Col} variant="primary" type="submit" onClick={this.sentPostRequest} >
                            Submit
                        </Button>

                        <Button as={Col} variant="danger" type="back" onClick={() => window.location.href = "http://localhost:3000"} >
                            Back
                        </Button>
                    </Form.Row>
                </Form>
                {this.state.succes === true && <div class="alert">
                    <span class="closebtn" onClick="this.parentElement.style.display='none';">&times;</span>
                    Succes!!
                </div>}
            </div>

        )



    }
}