import React from 'react';
import Button from 'react-bootstrap/Button';
import axios from 'axios';
import Form from 'react-bootstrap/Form';





export default class BookFlightComponent extends React.Component {

  constructor() {
    super()
    this.state = {
      seats: null
    }
    this.renderChanges = this.renderChanges.bind(this)
    this.sentPostRequest = this.sentPostRequest.bind(this)
  }


  sentPostRequest() {
    let info = { flightId: this.props.flightId, bookedSeats: this.state.seats }
    axios.post(`http://localhost:8080/api/bookings/`, info, { withCredentials: true })
      .then(response => console.log(response))
      .catch(err => console.error(err.response.data))
  }


  renderChanges(event) {
    this.setState({ seats: event.target.value });
  }



  render() {
    const numberOfSeats = Array.from(Array(this.props.flights + 1).keys())

    return (
      <div>
        <Button variant="primary" className='book' onClick={this.sentPostRequest}>Book
           </Button>
        <Form>
          <Form.Group controlId="exampleForm.ControlSelect1" >
            <Form.Label >Select number of seats</Form.Label>
            <Form.Control as="select" onChange={this.renderChanges}>
              {numberOfSeats.map(seat => {
                return <option key={seat + 1}>{seat}</option>
              })}
            </Form.Control>

          </Form.Group>
        </Form>
      </div>
    )
  }








}